//
//  ChatUseCase.swift
//  ChatApp
//
//  Created by Dung Do on 20/01/2022.
//

import Foundation
import RxSwift
import ConnectyCube

protocol ChatUseCase {
    // MARK: - Dialogs
    func createDialog() -> Observable<ChatDialog>
    func getDialogs(skip: Int, limit: Int) -> Observable<[ChatDialog]>
    func getDialog(from id: String) -> Observable<ChatDialog?>
    func join(dialog: ChatDialog) -> Completable
    func leave(dialog: ChatDialog) -> Completable
    func subscribeDialog(from id: String) -> Completable
    
    // MARK: - Chat
    func chat(content: String, dialog: ChatDialog) -> Observable<ChatMessage>
    func getMessages(from dialog: ChatDialog, skip: Int, limit: Int) -> Observable<[ChatMessage]>
    func observerMessage() -> Observable<ChatMessage>
}
