//
//  UserUseCase.swift
//  ChatApp
//
//  Created by Dung Do on 20/01/2022.
//

import Foundation
import RxSwift
import ConnectyCube

protocol UserUseCase {
    func createUser() -> Observable<User>
    func loginUser() -> Observable<User>
    func connect() -> Completable
    func disconnect() -> Completable
}
