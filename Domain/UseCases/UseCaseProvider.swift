//
//  UseCaseProvider.swift
//  ChatApp
//
//  Created by Dung Do on 20/01/2022.
//

import Foundation

protocol UseCaseProvider {
    func createUserUseCase() -> UserUseCase
    func createChatUseCase() -> ChatUseCase
}
