//
//  XMPPUseCaseProvider.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 21/02/2022.
//

import Foundation

class XMPPUseCaseProvider: UseCaseProvider {
    
    func createUserUseCase() -> UserUseCase {
        return XMPPUserUseCase()
    }
    
    func createChatUseCase() -> ChatUseCase {
        return XMPPChatUseCase()
    }
    
}
