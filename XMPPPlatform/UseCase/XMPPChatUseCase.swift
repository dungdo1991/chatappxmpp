//
//  XMPPChatUseCase.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 21/02/2022.
//

import Foundation
import ConnectyCube
import RxSwift

class XMPPChatUseCase: ChatUseCase {
    
    let xmpp = XMPPChat()
    
    // MARK: - Rooms
    
    func createDialog() -> Observable<ChatDialog> {
        xmpp.createDialog()
    }
    
    func getDialogs(skip: Int, limit: Int) -> Observable<[ChatDialog]> {
        xmpp.getDialogs(skip: skip, limit: limit)
    }
    
    func getDialog(from id: String) -> Observable<ChatDialog?> {
        xmpp.getDialog(from: id)
    }
    
    func join(dialog: ChatDialog) -> Completable {
        xmpp.joinDialog(dialog: dialog)
    }
    
    func leave(dialog: ChatDialog) -> Completable {
        xmpp.leaveDialog(dialog: dialog)
    }
    
    func subscribeDialog(from id: String) -> Completable {
        xmpp.subscribeDialog(from: id)
    }
    
    // MARK: - Chat
    
    func chat(content: String, dialog: ChatDialog) -> Observable<ChatMessage> {
        xmpp.chat(content: content, dialog: dialog)
    }
    
    func getMessages(from dialog: ChatDialog, skip: Int, limit: Int) -> Observable<[ChatMessage]> {
        xmpp.getMessages(from: dialog, skip: skip, limit: limit)
    }
    
    func observerMessage() -> Observable<ChatMessage> {
        xmpp.observerMessage()
    }
    
}
