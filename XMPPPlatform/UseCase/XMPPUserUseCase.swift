//
//  XMPPUserUseCase.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 21/02/2022.
//

import Foundation
import ConnectyCube
import RxSwift

class XMPPUserUseCase: UserUseCase {
    
    let xmpp = XMPPUser()
    
    func createUser() -> Observable<User> {
        xmpp.createUser()
    }
    
    func loginUser() -> Observable<User> {
        xmpp.loginUser()
    }
    
    func connect() -> Completable {
        xmpp.connect()
    }
    
    func disconnect() -> Completable {
        xmpp.disconnect()
    }
    
}
