//
//  XMPPUser.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 21/02/2022.
//

import Foundation
import ConnectyCube
import RxSwift

class XMPPUser {
    
    func createUser() -> Observable<User> {
        Observable.create { observer in
            let user = User()
            user.login = UUID().uuidString
            user.password = UUID().uuidString

            Request.signUp(user, successBlock: { user in
                observer.onNext(user)
            }) { (error) in
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
    
    func loginUser() -> Observable<User> {
        Observable.create { observer in
            Request.logIn(withUserLogin: Session.shared.currentLogin!, password: Session.shared.currentPassword!, successBlock: { user in
                observer.onNext(user)
            }) { (error) in
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
    
    func connect() -> Completable {
        Completable.create { observer in
            Chat.instance.connect(withUserID: Session.shared.currentID!, password: Session.shared.currentPassword!) { error in
                if let error = error {
                    observer(.error(error))
                } else {
                    observer(.completed)
                }
            }
            return Disposables.create()
        }
    }
    
    func disconnect() -> Completable {
        Completable.create { observer in
            Chat.instance.disconnect { error in
                if let error = error {
                    observer(.error(error))
                } else {
                    observer(.completed)
                }
            }
            return Disposables.create()
        }
    }
    
}
