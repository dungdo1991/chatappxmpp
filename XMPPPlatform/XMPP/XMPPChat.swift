//
//  XMPPChat.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 21/02/2022.
//

import Foundation
import ConnectyCube
import RxSwift

class XMPPChat: NSObject {
    
    var newMessage: ((ChatMessage)->())?
    
    override init() {
        super.init()
        Chat.instance.addDelegate(self)
    }
    
    // MARK: - Dialog
    
    func createDialog() -> Observable<ChatDialog> {
        Observable.create { observer in
            let dialog = ChatDialog(dialogID: nil, type: .public)
            dialog.name = "New public dialog"

            Request.createDialog(dialog, successBlock: { (dialog) in
                Request.subscribeToPublicDialog(withID: dialog.id!, successBlock: { dialog in
                    observer.onNext(dialog)
                }) { (error) in
                    observer.onError(error)
                }
            }) { (error) in
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
    
    func getDialogs(skip: Int, limit: Int) -> Observable<[ChatDialog]> {
        Observable.create { observer in
            let extRequest : [String: String] = ["sort_desc" : "lastMessageDate"]
            Request.dialogs(with: Paginator.limit(UInt(limit), skip: UInt(skip)),
                            extendedRequest: extRequest,
                            successBlock: { (dialogs, usersIDs, paginator) in
                observer.onNext(dialogs)
            }) { (error) in
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
    
    func getDialog(from id: String) -> Observable<ChatDialog?> {
        Observable.create { observer in
            let extRequest : [String: String] = [
                "sort_desc" : "lastMessageDate",
                "_id[in]" : id
            ]
            
            Request.dialogs(with: Paginator.limit(1, skip: 0),
                            extendedRequest: extRequest,
                            successBlock: { (dialogs, usersIDs, paginator) in
                observer.onNext(dialogs.first)
            }) { (error) in
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
    
    func joinDialog(dialog: ChatDialog) -> Completable {
        Completable.create { observer in
            dialog.join { error in
                if let error = error {
                    observer(.error(error))
                } else {
                    observer(.completed)
                }
            }
            return Disposables.create()
        }
    }
    
    func leaveDialog(dialog: ChatDialog) -> Completable {
        Completable.create { observer in
            dialog.leave { error in
                if let error = error {
                    observer(.error(error))
                } else {
                    observer(.completed)
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeDialog(from id: String) -> Completable {
        Completable.create { observer in
            Request.subscribeToPublicDialog(withID: id) { _ in
                observer(.completed)
            } errorBlock: { error in
                observer(.error(error))
            }
            return Disposables.create()
        }
    }
    
    // MARK: - Chat
    
    func chat(content: String, dialog: ChatDialog) -> Observable<ChatMessage> {
        Observable.create { observer in
            let message = ChatMessage()
            message.text = content

            dialog.send(message) { error in
                if let error = error {
                    observer.onError(error)
                } else {
                    observer.onNext(message)
                }
            }
            return Disposables.create()
        }
    }
    
    func getMessages(from dialog: ChatDialog, skip: Int, limit: Int) -> Observable<[ChatMessage]> {
        Observable.create { observer in
            Request.messages(withDialogID: dialog.id!,
                             extendedRequest: nil,
                             paginator: Paginator.limit(UInt(limit), skip: UInt(skip)),
                             successBlock: { messages, paginator in
                observer.onNext(messages)
            }) { error in
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
    
    func observerMessage() -> Observable<ChatMessage> {
        Observable.create { observer in
            self.newMessage = { message in
                observer.onNext(message)
            }
            return Disposables.create()
        }
    }
    
}

extension XMPPChat: ChatDelegate {
    
    func chatRoomDidReceive(_ message: ChatMessage, fromDialogID dialogID: String) {
        newMessage?(message)
    }
    
}
