//
//  OtherChatTableViewCell.swift
//  ChatApp
//
//  Created by Dung Do on 27/01/2022.
//

import UIKit
import ConnectyCube

class OtherChatTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var message: ChatMessage? {
        didSet {
            contentLabel.text = message?.text
            nameLabel.text = "\(message?.senderID ?? 0)"
        }
    }
    
}
