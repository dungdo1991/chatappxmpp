//
//  MyChatTableViewCell.swift
//  ChatApp
//
//  Created by Dung Do on 27/01/2022.
//

import UIKit
import ConnectyCube

class MyChatTableViewCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    
    var message: ChatMessage? {
        didSet {
            contentLabel.text = message?.text
        }
    }
    
}
