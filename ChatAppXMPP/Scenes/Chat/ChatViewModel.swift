//
//  ChatViewModel.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 21/02/2022.
//

import Foundation
import ConnectyCube
import RxSwift
import RxCocoa

class ChatViewModel {
    
    private let navigator: ChatNavigator
    private let userUseCase: UserUseCase
    private let chatUseCase: ChatUseCase
    private let dialog: ChatDialog
    
    init(navigator: ChatNavigator, userUseCase: UserUseCase, chatUseCase: ChatUseCase, dialog: ChatDialog) {
        self.navigator = navigator
        self.userUseCase = userUseCase
        self.chatUseCase = chatUseCase
        self.dialog = dialog
    }
    
    deinit {
        print("deinit - ChatViewModel")
    }
    
    func tranform(input: Input) -> Output {
        let errorTracker = ErrorTracker()
        let activityIndicator = ActivityIndicator()
        var messages = [ChatMessage]()
        
        let nextPage = input.nextPageTrigger
            .flatMapLatest { _ -> Driver<[ChatMessage]> in
                return self.chatUseCase.getMessages(from: self.dialog, skip: messages.count, limit: 15)
                    .track(activity: activityIndicator, error: errorTracker)
            }
            .map { newMessages -> [ChatMessage] in
                messages.append(contentsOf: newMessages)
                return messages
            }
        
        let sentMessage = input.tapSend
            .withLatestFrom(input.message)
            .flatMapLatest { message -> Driver<Void> in
                if message.isEmpty {
                    return Driver.empty()
                } else {
                    return self.chatUseCase.chat(content: message, dialog: self.dialog)
                        .track(activity: activityIndicator, error: errorTracker)
                        .mapToVoid()
                }
            }
        
        let viewWillAppear = input.viewWillAppear
            .flatMapLatest { _ -> Driver<Void> in
                return self.userUseCase.connect()
                    .andThen(Observable.just(()))
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()
            }
            .flatMapLatest { _ -> Driver<Void> in
                return self.chatUseCase.join(dialog: self.dialog)
                    .andThen(Observable.just(()))
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()
            }
        
        let viewWillDisappear = input.viewWillDisappear
            .flatMapLatest { _ -> Driver<Void> in
                return self.chatUseCase.leave(dialog: self.dialog)
                    .andThen(self.userUseCase.disconnect())
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()
                    .mapToVoid()
            }
        
        let newMessage = input.viewWillAppear
            .flatMapLatest { _ -> Driver<[ChatMessage]> in
                return self.chatUseCase.observerMessage()
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
                    .map { newMessage -> [ChatMessage] in
                        messages.insert(newMessage, at: 0)
                        return messages
                    }
            }
        
        return Output(error: errorTracker.asDriver(),
                      loading: activityIndicator.asDriver(),
                      messages: Driver.merge(nextPage, newMessage),
                      sentMessage: sentMessage,
                      actionComplete: Driver.merge(viewWillAppear, viewWillDisappear))
    }
}

extension ChatViewModel {
    struct Input {
        let viewWillAppear: Driver<Void>
        let viewWillDisappear: Driver<Void>
        let nextPageTrigger: Driver<Void>
        let message: Driver<String>
        let tapSend: Driver<Void>
    }
    struct Output {
        let error: Driver<Error>
        let loading: Driver<Bool>
        let messages: Driver<[ChatMessage]>
        let sentMessage: Driver<Void>
        let actionComplete: Driver<Void>
    }
}
