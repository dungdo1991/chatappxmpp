//
//  ChatNavigator.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 21/02/2022.
//

import UIKit

protocol ChatNavigator: AnyObject {
    func goDialogs()
}

class DefaultChatNavigator: ChatNavigator {
    
    private let navigation: UINavigationController
    private var useCaseProvider: UseCaseProvider
    
    init(navigation: UINavigationController, useCaseProvider: UseCaseProvider) {
        self.navigation = navigation
        self.useCaseProvider = useCaseProvider
    }
    
    func goDialogs() {
        navigation.popViewController(animated: true)
    }
    
}
