//
//  DialogsViewModel.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 21/02/2022.
//

import Foundation
import RxSwift
import RxCocoa
import ConnectyCube

class DialogsViewModel: ViewModelType {
    
    private let navigator: DialogsNavigator
    private let userUseCase: UserUseCase
    private let chatUseCase: ChatUseCase
    
    init(userUseCase: UserUseCase, chatUseCase: ChatUseCase, navigator: DialogsNavigator) {
        self.userUseCase = userUseCase
        self.chatUseCase = chatUseCase
        self.navigator = navigator
    }
    
    func tranform(input: Input) -> Output {
        let errorTracker = ErrorTracker()
        let activityIndicator = ActivityIndicator()
        var dialogs = [ChatDialog]()
        
        let datas = Driver.combineLatest(input.searchDialog, input.nextPageTrigger)
            .flatMapLatest { text, _ -> Driver<[ChatDialog]> in
                if text.isEmpty {
                    return self.chatUseCase.getDialogs(skip: 0, limit: 15)
                        .track(activity: activityIndicator, error: errorTracker)
                        .do { newDialogs in
                            dialogs = newDialogs
                        }
                } else {
                    return self.chatUseCase.getDialog(from: text)
                        .trackError(errorTracker)
                        .asDriverOnErrorJustComplete()
                        .map { dialog -> [ChatDialog] in
                            if let dialog = dialog {
                                return [dialog]
                            } else {
                                return []
                            }
                        }
                        .do { dialog in
                            dialogs.removeAll()
                            dialogs.append(contentsOf: dialog)
                        }
                }
            }
        
        let tapRoom = input.tapDialog
            .flatMapLatest { indexPath in
                self.chatUseCase.subscribeDialog(from: dialogs[indexPath.row].id!)
                    .andThen(Observable.just(indexPath))
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }
            .do { indexPath in
                self.navigator.goToChat(dialog: dialogs[indexPath.row])
            }
            .mapToVoid()
        
        let tapCreateDialog = input.tapCreateDialog
            .flatMapLatest { _ in
                self.chatUseCase.createDialog()
                    .track(activity: activityIndicator, error: errorTracker)
            }
            .do { dialog in
                self.navigator.goToChat(dialog: dialog)
            }
            .map { dialog -> [ChatDialog] in
                dialogs.insert(dialog, at: 0)
                return dialogs
            }
        
        let viewWillAppear = input.viewWillAppear
            .flatMapLatest { _ -> Driver<Void> in
                if Session.shared.currentLogin == nil {
                    return self.userUseCase.createUser()
                        .trackError(errorTracker)
                        .do { user in
                            Session.shared.currentID = user.id
                            Session.shared.currentLogin = user.login
                            Session.shared.currentPassword = user.password
                        }
                        .flatMapLatest { _ in
                            self.userUseCase.loginUser()
                                .track(activity: activityIndicator, error: errorTracker)
                        }
                        .asDriverOnErrorJustComplete()
                        .mapToVoid()
                } else {
                    return self.userUseCase.loginUser()
                        .track(activity: activityIndicator, error: errorTracker)
                        .mapToVoid()
                }
            }
        
        let newMessage = input.viewWillAppear
            .flatMapLatest { _ in
                self.chatUseCase.observerMessage()
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
                    .map { newMessage -> [ChatDialog] in
                        if let index = dialogs.firstIndex(where: { $0.id == newMessage.dialogID }) {
                            let dialog = dialogs.remove(at: index)
                            dialogs.insert(dialog, at: 0)
                        }
                        return dialogs
                    }
            }
        
        let actionComplete = Driver.merge(tapRoom, viewWillAppear)
        
        return Output(error: errorTracker.asDriver(),
                      loading: activityIndicator.asDriver(),
                      dialogs: Driver.merge(datas, tapCreateDialog, newMessage),
                      actionComplete: actionComplete)
    }
    
}

extension DialogsViewModel {
    struct Input {
        let viewWillAppear: Driver<Void>
        let nextPageTrigger: Driver<Void>
        let tapDialog: Driver<IndexPath>
        let tapCreateDialog: Driver<Void>
        let searchDialog: Driver<String>
    }
    struct Output {
        let error: Driver<Error>
        let loading: Driver<Bool>
        let dialogs: Driver<[ChatDialog]>
        let actionComplete: Driver<Void>
    }
}
