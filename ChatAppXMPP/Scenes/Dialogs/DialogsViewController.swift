//
//  DialogsViewController.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 16/02/2022.
//

import UIKit
import PKHUD
import RxSwift
import RxCocoa
import ConnectyCube

class DialogsViewController: BaseViewController {
    
    @IBOutlet weak var dialogsTableView: UITableView!
    @IBOutlet weak var createButton: UIButton!
    
    var dialogsVM: DialogsViewModel?
    private lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        return searchBar
    }()
    
    deinit {
        print("deinit - DialogsViewController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupRx()
        dialogsTableView.setContentOffset(CGPoint(x: 0, y :CGFloat.greatestFiniteMagnitude), animated: false)
    }
    
    private func setupView() {
        title = "Dialogs"
        
        dialogsTableView.registerClass(UITableViewCell.self)
        dialogsTableView.delegate = self
        
        navigationItem.titleView = searchBar
    }
    
    private func setupRx() {
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = DialogsViewModel.Input(viewWillAppear: viewWillAppear,
                                         nextPageTrigger: dialogsTableView.rx.reachedBottom,
                                         tapDialog: dialogsTableView.rx.itemSelected.asDriver(),
                                         tapCreateDialog: createButton.rx.tap.asDriver(),
                                         searchDialog: searchBar.rx.text.orEmpty.asDriver())
        
        let output = dialogsVM?.tranform(input: input)
        output?.error
            .drive(errorBinding)
            .disposed(by: disposeBag)
        output?.loading
            .drive(loadingBinding)
            .disposed(by: disposeBag)
        output?.dialogs
            .drive(dialogsTableView.rx.items(cellIdentifier: UITableViewCell.reuseID, cellType: UITableViewCell.self)) { index, room, cell in
                cell.selectionStyle = .none
                cell.textLabel?.text = room.id
            }
            .disposed(by: disposeBag)
        output?.actionComplete
            .drive()
            .disposed(by: disposeBag)
    }
    
}

extension DialogsViewController: UITableViewDelegate {
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}

extension DialogsViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
}
