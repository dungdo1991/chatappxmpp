//
//  DialogsNavigator.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 21/02/2022.
//

import UIKit
import ConnectyCube

protocol DialogsNavigator: AnyObject {
    func goToChat(dialog: ChatDialog)
}

class DefaultDialogsNavigator: DialogsNavigator {
    
    private let navigation: UINavigationController
    private let useCaseProvider: UseCaseProvider
    
    init(navigation: UINavigationController, useCaseProvider: UseCaseProvider) {
        self.navigation = navigation
        self.useCaseProvider = useCaseProvider
    }
    
    func goToChat(dialog: ChatDialog) {
        let nav = DefaultChatNavigator(navigation: navigation, useCaseProvider: useCaseProvider)
        let viewModel = ChatViewModel(navigator: nav,
                                      userUseCase: useCaseProvider.createUserUseCase(),
                                      chatUseCase: useCaseProvider.createChatUseCase(),
                                      dialog: dialog)
        let chatVC = ChatViewController()
        chatVC.chatVM = viewModel
        navigation.pushViewController(chatVC, animated: true)
    }
    
}
