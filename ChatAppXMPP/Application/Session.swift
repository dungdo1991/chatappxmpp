//
//  Session.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 16/02/2022.
//

import Foundation

class Session {
    
    static let shared = Session()
    
    private init() {}
    
    private var id: UInt?
    var currentID: UInt? {
        get {
            if let data = UserDefaults.standard.data(forKey: "currentID"),
               let id = try? JSONDecoder().decode(UInt.self, from: data) {
                return id
            }
            return id
        }
        set {
            id = newValue
            let data = try! JSONEncoder().encode(newValue)
            UserDefaults.standard.set(data, forKey: "currentID")
        }
    }
    
    private var login: String?
    var currentLogin: String? {
        get {
            if let data = UserDefaults.standard.data(forKey: "currentLogin"),
               let login = try? JSONDecoder().decode(String.self, from: data) {
                return login
            }
            return login
        }
        set {
            login = newValue
            let data = try! JSONEncoder().encode(newValue)
            UserDefaults.standard.set(data, forKey: "currentLogin")
        }
    }
    
    private var password: String?
    var currentPassword: String? {
        get {
            if let data = UserDefaults.standard.data(forKey: "currentPassword"),
               let password = try? JSONDecoder().decode(String.self, from: data) {
                return password
            }
            return password
        }
        set {
            password = newValue
            let data = try! JSONEncoder().encode(newValue)
            UserDefaults.standard.set(data, forKey: "currentPassword")
        }
    }
    
}
