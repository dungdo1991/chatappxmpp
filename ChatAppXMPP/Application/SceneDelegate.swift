//
//  SceneDelegate.swift
//  ChatAppXMPP
//
//  Created by Dung Do on 16/02/2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = (scene as? UIWindowScene) else { return }
        
        let dialogsVC = DialogsViewController()
        let nav = UINavigationController(rootViewController: dialogsVC)
        let provider = XMPPUseCaseProvider()
        let navigator = DefaultDialogsNavigator(navigation: nav, useCaseProvider: provider)
        dialogsVC.dialogsVM = DialogsViewModel(userUseCase: provider.createUserUseCase(),
                                               chatUseCase: provider.createChatUseCase(),
                                               navigator: navigator)
        window = UIWindow(windowScene: scene)
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
    }

}

