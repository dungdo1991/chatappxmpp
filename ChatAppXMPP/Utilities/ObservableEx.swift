//
//  ObservableEx.swift
//  cleanarchitecture
//
//  Created by Dung Do on 05/01/2022.
//

import Foundation
import RxSwift
import RxCocoa

extension ObservableType {
    
    func asDriverOnErrorJustComplete() -> Driver<Element> {
        return asDriver { error in
            return Driver.empty()
        }
    }
    
    func mapToVoid() -> Observable<Void> {
        return map { _ in }
    }
    
    func unwrap<T>() -> Observable<T> where Element == T? {
        return self.filter { $0 != nil }.map { $0! }
    }
    
    func track(activity: ActivityIndicator, error: ErrorTracker) -> Driver<Element> {
        return self.trackActivity(activity)
            .trackError(error)
            .asDriverOnErrorJustComplete()
    }
    
}

extension SharedSequenceConvertibleType {
    
    func mapToVoid() -> SharedSequence<SharingStrategy, Void> {
        return map { _ in }
    }
    
    func unwrap<T>() -> SharedSequence<SharingStrategy, T> where Element == T? {
        return self.filter { $0 != nil }.map { $0! }
    }
    
}
